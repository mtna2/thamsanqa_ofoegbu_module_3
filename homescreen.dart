import 'package:flutter/material.dart';
import 'package:thamsanqa_ofoegbu_module_3/src/pages/loginscreen.dart';

class homescreen extends StatelessWidget {
  const homescreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text('home screen'),
      ),
      body: Center(
        //have two buttons
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              color: Colors.blue,
              child: const Text('Go To login'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const loginscreen();
                    },
                  ),
                );
                //Navigate to Screen 1
              },
            ),
            RaisedButton(
              color: const Color.fromARGB(255, 159, 33, 243),
              child: const Text('Register'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return registerscreen();
                    },
                  ),
                );
                //Navigate to resigterScreen
              },
            ),
            /*RaisedButton(
              color: Colors.purple,
              child: Text('Go To userprofile'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return userprofile();
                    },
                  ),
                );
                //Navigate to Screen 3
              },
            ), */
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return userprofile();
              },
            ),
          );
        },
        child: const Icon(
          Icons.people,
          color: Color.fromARGB(255, 123, 7, 255),
        ),
        backgroundColor: Colors.white70,
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniStartDocked,
    );
  }

  Widget registerscreen() {}

  Widget userprofile() {}
}
// ( i ran outof time my applications dont work well)
