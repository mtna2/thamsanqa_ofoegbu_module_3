import 'package:flutter/material.dart';

class user_profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Userprofile'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            color: Color.fromARGB(255, 181, 74, 195),
            height: MediaQuery.of(context).size.height * 0.3,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 50,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("thamsanqa Ofoegbu"),
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[Text("Posts"), Text("455")],
                  ),
                  Column(
                    children: <Widget>[Text("Followers"), Text("300")],
                  ),
                  Column(
                    children: <Widget>[Text("Following"), Text("237")],
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              child: Column(
                children: <Widget>[
                  Text("Information"),
                  Divider(),
                  ListTile(
                    title: Text("Location"),
                    subtitle: Text("South Africa"),
                    leading: Icon(Icons.location_on),
                  ),
                  ListTile(
                    title: Text("Email"),
                    subtitle: Text("jacaland@gmail.com"),
                    leading: Icon(Icons.email),
                  ),
                  ListTile(
                    title: Text("Phone number"),
                    subtitle: Text("067452367"),
                    leading: Icon(Icons.phone),
                  ),
                  ListTile(
                    title: Text("culture:"),
                    subtitle: Text("mixed, half xhosa  half tswana"),
                    leading: Icon(Icons.info),
                  ),
                ],
              ),
            ),
          ),
        ],
        /*child: RaisedButton(
          color: Colors.purle,
          child: Text('Go Back To home page'),
          onPressed: () {
            Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return homescreen();( i ran outof time my applications dont work well)
                    },
                  ),
                );
          },
        ), */
      ),
    );
  }
}
