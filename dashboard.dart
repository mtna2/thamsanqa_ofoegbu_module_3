import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(const dashboard());
}

class dashboard extends StatefulWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  State<dashboard> createState() => _dashboardState();
}

class _DashboardState extends State<dashboard> {
  Material myItems(IconData icon, String heading, int color) {
    return Material(
      color: Colors.white,
      elevation: 14,
      shadowColor: const Color.fromARGB(128, 124, 33, 243),
      borderRadius: BorderRadius.circular(24),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      heading,
                      style: TextStyle(color: Color(color), fontSize: 24),
                    ),
                  ),
                  Material(
                    color: Color(color),
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Icon(
                        icon,
                        color: Colors.white,
                        size: 35,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard'),
          centerTitle: true,
          backgroundColor: Colors.teal,
        ),
        body: StaggeredGridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 30,
          mainAxisSpacing: 30,
          padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 35),
          children: <Widget>[
            myItems(Icons.local_attraction, 'Track us!', 0xFF000000),
            myItems(Icons.money, 'Get our Service!', 0xFF000000),
            myItems(Icons.settings, 'Settings', 0xFF000000),
            myItems(Icons.account_circle, 'Profile', 0xFF000000),
          ],
          staggeredTiles: [
            StaggeredTile.extent(1, 150),
            StaggeredTile.extent(1, 150),
            StaggeredTile.extent(1, 150),
            StaggeredTile.extent(1, 150),
          ],
        ),
      ),
    );
  }
}

class StaggeredTile {
  static extent(int i, int j) {}
}

class StaggeredGridView {
  static count(
      {int crossAxisCount,
      int crossAxisSpacing,
      int mainAxisSpacing,
      EdgeInsets padding,
      List<Widget> children,
      List staggeredTiles}) {}
}
// ( i ran outof time my applications dont work well)